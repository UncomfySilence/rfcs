- Feature Name: Souls and population dynamics
- Start Date: 2021-06-13
- RFC Number: (leave this empty for now - the core developers will give you a number to put here when the RFC is submitted)
- Tracking Issue: (leave this empty - the core developers will give you a number to put here when the RFC is submitted)

# Summary
[summary]: #summary

RtSim entities will be given the capability to respawn, and this will be themed as "having souls". Non-RtSim entities will be tracked as quantities with population dynamics (coarser than RtSim).

# Motivation
[motivation]: #motivation

Currently, players respawn, but there's no in-universe explanation as to why, and its an asymmetry with NPCs who are player-like, but nonetheless don't respawn.
At the same time, for dungeons to fulfil their standard RPG role of being an input to the economy that provides loot (with dungeoneering as a profession), dungeon enemies need to not be subject to a player-like respawning mechanism.
Additionally, it would be nice simulation-wise if predator-prey dynamics were modeled, such that quests to solve mundane overpopulation/underpopulation were derived from a realistic model, and players/NPCs could, for example, swell rabbit populations by culling too many wolves.

In order to have immortal/respawning pets, there needs to be some sort of "ensouling" ritual for bestowing souls on previously unsouled animals.
Dually, rituals that "unensoul" creatures give some candidate lore-level answers for the cultist's motivations, which potentially leads into future features.

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation


## Ensouled entities

Villagers, adventurers (whether they be players or NPCs), merchants, and guards have souls and respawn at their last waypoint when they die.
Wild animals and cultists don't have souls, and don't respawn when they die.

## Ensouling

You can get peaceful wild animals to follow you by [TODO: whatever the long-term replacement for collar is].
While animals are following you, they don't yet have a soul, they can still permenantly die, and they despawn when you log out.
You can ensoul an animal that's been following you enough by [TODO: ritual and interface to said ritual goes here], making it a part of you that will respawn near you when it dies, and pop in and out of existence with you when you log in and out.

## Population dynamics

The population dynamics of wild animals are simulated.
Predators need prey to sustain them, and prey need plants to sustain them.
Killing predators can lead to overpopulation of the types of prey they hunt, and killing prey can cause their corresponding predator populations to decline.

## Unensouling

Cultists (and dungeon enemies in general) have given up their souls (and hence their immortality/respawn capability) in dark rituals.
Some cultists preach the virtues of mortality, having already lived as immortals for millenia.
Other cultists are seeking higher levels of power with a return to immortality at a cost of temporary mortality risk through these rituals.
### Permadeath
If player characters are allowed to join the cultist faction and do the rituals, the character that does the ritual becomes subject to permadeath.
If there's some ritual that re-ensouls them with more power (e.g. as some sort of demon lord), that unsets their permadeath (but may come with other downsides, like a permenant waypoint deep in a dungeon).

# Reference-level explanation
[reference-level-explanation]: #reference-level-explanation

## Ensouled respawning

RtSim entities need to be created with an additional field indicating where they should respawn if they should respawn (a waypoint), probably as something like `Option<Vec2<f32>>`. On death, they should be moved to their respawn point (or possibly enqueued to respawn with a random delay).

## Population dynamics

Population dynamics are intended to be done with [Generalized Lotka-Volterra](https://en.wikipedia.org/wiki/Generalized_Lotka%E2%80%93Volterra_equation) which keeps track of population per animal type in some cell size that may correspond to chunks, or be a multiple of chunk size, mixed with a cellular automaton travel model for modeling migration of population between cells. This probably doesn't need to be done every tick.

## Ensouling and de-ensouling rituals

To be left to a future RFC, since it nontrivially interacts with pets/familiars.

# Drawbacks
[drawbacks]: #drawbacks

## TODO
Why should we *not* do this?

This section is *really* important. An RFC is more likely to be accepted if it recognises its own weaknesses and attempts to consider them as part of the proposal.

Remember: Listing drawbacks doesn't mean an RFC won't be accepted. If anything, it gives developers the confidence to move forward with the RFC, knowing what impact it may have.

# Rationale and alternatives
[alternatives]: #alternatives

## Why is this design the best in the space of possible designs?
This is a synthesis of what was discussed on Discord starting at https://discord.com/channels/449602562165833758/467861297279533066/853637715135168568, and intends to reflect consensus.

## What other designs have been considered and what is the rationale for not choosing them?
### The status quo 
- Chunk reloading for non-rtsim entities with no population dynamics
- Rtsim entities die permenantly

Rtsim entities dying permenantly is an inconsistency, and makes it harder to add features like Rtsim adventuring parties.

### Player permadeath
Would be consistent with Rtsim permadeath, but is relatively undesirable as a default

## What is the impact of not doing this?
TODO

# Prior art
[prior-art]: #prior-art

## TODO
Discuss prior art, both the good and the bad, in relation to this proposal.
A few examples of what this can include are:

- Does this feature exist in other games or engines and what experience have their community had?
- For community proposals: Is this done by some other community and what were their experiences with it?
- For other teams: What lessons can we learn from what other communities have done here?
- Papers: Are there any published papers or great posts that discuss this? If you have some relevant papers to refer to, this can serve as a more detailed theoretical background.

This section is intended to encourage you as an author to think about the lessons from other games: provide readers of your RFC with a fuller picture.
If there is no prior art, that is fine - your ideas are interesting to us whether they are brand new or if it is an adaptation from other projects.

Note that while precedent set by other games is some motivation, it does not on its own motivate an RFC.
Please also take into consideration that Veloren sometimes intentionally diverges from common game features.

# Unresolved questions
[unresolved]: #unresolved-questions

- Details of pets/familiars
- Details of the rituals that cultists do
- - If cultists still use chunk-reloading, how does this tie into the ritual that severed their souls?
- How food interacts with resurrective immortality for the purposes of econsim
