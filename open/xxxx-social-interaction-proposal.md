# Goal:  

To propose an individual interaction between a player and an NPC that is more than a simple speech level check to determine effectiveness.  This is primarily a system that would interact with all aspects of the social domain whenever interacting with any NPC, be it bartering, getting rumors and information, hiring individuals, and convincing and bluffing situations.

# Design Criteria:

1. Implement a system that requires player thought when interacting with an NPC.  This thought can be knowledge of lore or understanding the system itself.

2. Have this be applicable to any interaction with an NPC in order to accomplish a goal. Goals can include bartering, training, hiring NPC’s, taunting, threatening, and bluffing

3. Add a gameplay element outside of a numeric speech check to be successful of an interaction.

# Core Concepts:

1. Reputation is a faction based disposition towards an individual player.  For simplicity sake, let's say it is a scale from 0, where an NPC is attack on sight, to 100 where the player is revered and considered a paragon. 

2. Social Skill is a player’s numeric value of their social ability.

3. NPC Disposition is an individual NPC’s relation to the player.  This is linked to the player’s faction Reputation that the NPC is associated with.  The Disposition of the NPC will be the same as the faction’s reputation when initiating a social interaction.

4. The Greeting is the initial interaction with an NPC where a faction’s appropriate greeting may be given which will either decrease or increase an NPC’s Disposition depending on the player’s goal.

5. The interaction is the second step of the interaction.  Interactions are taunt, cower, compliment, bribe, and neutral.  This will be a modifier for the final step of the NPC interaction.  Specific interactions are associated with a desired action but can harm other actions if the wrong interaction is chosen.

6. The desired action is what the player wants the NPC to do.  This is where a speech check will be done but takes into account the disposition and the interaction modifier, and the players base social skill level to determine the success of the speech check. Desired actions can be barter, get trade information, get rumor, bluff and lie, start fight.

# Examples:

Player wants to trade with a merchant.  Player uses the proper greeting, compliments the merchant to get a bonus modifier to trade, and trades with the merchant with better prices.

Player wants to sneak into a bandit camp.  Player knows the passphrase greeting, cowers in front of the guard, and fulfills the desired action of bluffing.

Player wants to stir up trouble in a rival town.  Player uses the wrong greeting to lower the disposition, taunts the NPC to get an even lower disposition, and attempts the desired action to be getting into a fight.

