Feature Name: battlepets_through_weapons

Start Date: 2022-2-20

RFC Number: XXXX

Tracking Issue: XXXX



Summary

Pets will be linked to a weapon type for their species allowing for a maximum of 2 pets, one for each hand. 


Motivation

Linking animals to weapons instead of collars or other common feeding/taming techniques is to limit the amount of entities achievable logically to hinder the creation of large zoos and to further balance the notion of pets, slightly special casing them through skilltress and trust bindings. It would also allow for interesting choices and "class" based combination, i.e. interacting with enemies primarily through another entity, slightly controlling them, or combining a single entity with oneself, dealing damage, healing, or defending with a 1h weapon while controlling your partner in crime, your pet. 

Guide-level explanation

Explain the proposal as if it was already included in the game and you were explaining it to another player / developer. That generally means:
- Introducing new named concepts:

A new pet based weapontype, pet weapon skilltree, pet trust and training, pet commands though weapons, and abstraction of pet death/respawning.

- Explaining the feature largely in terms of examples:

There would be a multitude of weapon types with their own pet commands (attack, retreat, etc), one for each entity bodytype (ex: bird_large) or entitytype (ex: feathery) with different models to discern between them; Mammalian animals would be tied to a horn, Birds to a whistle, a conch for Reptilians, etc... (rest is to be decided by @Gemu [GMT + 2}]).

You can tame animals by feeding them certain foods befitting of their species. Animals may have different taming methods, but the most common method may be to throw food on the ground in hopes to lure an animal over. Aggressive animals might need to be soothed first by using stuff like pleasant scent bombs made from herbs, flowers etc that calm the animal down and make it approachable to be fed. As the entitiy/animal eats you would play the correct instruments (if it's a mammal you must play the horn) so that the animal may associate food and reward with the sound it makes. Thus, you can play different notes (abilities) and the animal would respond accordingly with the concept of a reward. You can bond with your animal by, as an example, winning fights and feeding it afterwars. This will build trust/relationship points between the owner and the pet. 

When a pet's HP bar reaches zero, it wont "die" but is implied to have fled. You can try to call it back using your pet weapon after battle. If your bond/trust has reached a certain level, it will have recognised you as its partner and come back. If your pet has only just been tamed and its already defeated, it'll get scared and you might lose it. 

- Explaining how Veloren players and developers should think about the feature, and how it should impact the way they play Veloren. It should explain the impact as concretely as possible:

This method of taming and handling animal allows for interesting interactions, forcing players into further decisions between empathy, resource management, combat, and partially transport (mounted transport would still be possible even without battlepets through purchasable animals, linked to stable owners, in towns).

- If applicable, describe the differences between how existing and new Veloren players / developers would adapt to the changes:

As the mounting/pets system is still in its infancy developers would be relatively unaffected outside of the creation of stables in towns (worldgen) and their economic purpose. Additionally, developers would have to work on the pet AI, a Trust/Bond system, and an effective taming system while allocating what certain animals can and can't eat for taming. 

Players, on the other hand, would treat pets as an important creature, and potentially a friend, as they're investing resources into the entity to make it better and to build trust to unlock certain features, like mounting (varies depending on the species).


Reference-level explanation

This is the technical portion of the RFC. Explain the design in sufficient detail that:
- Its interaction with other features is clear.
- It is reasonably clear how the feature would be implemented.
- Corner cases are dissected by example.

The section should return to the examples given in the previous section, and explain more fully how the detailed proposal makes those examples work.

(Adding in my opinion regarding what should be done, but then again, not a developer)

It may be best to start by implementing an effective healing system for entities, particulalry pets, by making entities pick up the drops of their kill. Additionally, it may be benificial to change the pet agent to be more self aware and capable of retreating/not instigating to increase its initial survivability. Afterwards, a more overhaul esc course of action may be needed, in which one large MR would be created to add in all the pet weapontype features that would be needed, like the weapon itself, abilities, animal response, and trust/bond. Afterwards, AI should be able to use pets themselves, mounting and attacking players/hostiles with their own entities. (Refer back to Guide-level explanation for further detail and functions)


Drawbacks

- Why should we not do this?
- This section is really important. An RFC is more likely to be accepted if it recognises its own weaknesses and attempts to consider them as part of the proposal.
- Remember: Listing drawbacks doesn't mean an RFC won't be accepted. If anything, it gives developers the confidence to move forward with the RFC, knowing what impact it may have.

Until the implementation of stables players would practically be forced to invest time and resources to obtain any marginally fast travel, which becomes more of an issue once we further restrict weapon swapping (weapons can't be swapped through hotbar or it takes more time to switch from bag). 


Rationale and alternatives

- Why is this design the best in the space of possible designs?
- What other designs have been considered and what is the rationale for not choosing them?
- What is the impact of not doing this?

This RFC is one of the better concepts as it creates an interesting sense of choice, limits pets to hinder large swaves of entities (similar to simple summons like a necro), and can be used as a resource sink (feeding pets).
Not using this concept isn't a detriment to the game, especially if a better concept is unveiled. 


Prior art

None that I know of, potential examples would be restricted necromancers in other MMOs, only capable of spawning a few entities but powerful nonetheless. 


Unresolved questions

 - What parts of the design do you expect to resolve through the RFC process before this gets merged?

 Whether or not we should use bodytype or entitytype for the weapons allocation. What weapon models would be used for each (body/entity)type. 

- What parts of the design do you expect to resolve through the implementation of this feature before stabilization?

Above stated answer^

- What related issues do you consider out of scope for this RFC that could be addressed in the future independently of the solution that comes out of this RFC?

Worldgen for animal stables and how to (de)load dead animals that have been abstracted as fleeing. 
